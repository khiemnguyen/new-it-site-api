'use strict';

// Category-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
  _id: Number,
  name: {
    type: String,
    required: true
  },
  parrent_id: Number,
  createdAt: {
    type: Date,
    'default': Date.now
  },
  updatedAt: {
    type: Date,
    'default': Date.now
  }
});

const CategoryModel = mongoose.model('Category', CategorySchema);

module.exports = CategoryModel;

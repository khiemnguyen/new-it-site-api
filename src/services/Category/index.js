'use strict';

const service = require('feathers-mongoose');
const Category = require('./Category-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: Category,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/Categories', service(options));

  // Get our initialize service to that we can bind hooks
  const CategoryService = app.service('/Categories');

  // Set up our before hooks
  CategoryService.before(hooks.before);

  // Set up our after hooks
  CategoryService.after(hooks.after);
};

'use strict';

// Event-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EventSchema = new Schema({
  _id: Number,
  title: { type: String, required: true },
  des: String,
  location: String,
  date: Date,
  images:[{
    alt: String,
    des: String,
    url: String
  }],
  state: String, //
  created_by: Number, // user id
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

const EventModel = mongoose.model('Event', EventSchema);

module.exports = EventModel;
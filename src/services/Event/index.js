'use strict';

const service = require('feathers-mongoose');
const Event = require('./Event-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: Event,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/Events', service(options));

  // Get our initialize service to that we can bind hooks
  const EventService = app.service('/Events');

  // Set up our before hooks
  EventService.before(hooks.before);

  // Set up our after hooks
  EventService.after(hooks.after);
};

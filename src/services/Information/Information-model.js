'use strict';

// Information-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InformationSchema = new Schema({
  _id: Number,
  text: { type: String, required: true }, // rich text or html
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

const InformationModel = mongoose.model('Information', InformationSchema);

module.exports = InformationModel;
'use strict';

const service = require('feathers-mongoose');
const Information = require('./Information-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: Information,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/Information', service(options));

  // Get our initialize service to that we can bind hooks
  const InformationService = app.service('/Information');

  // Set up our before hooks
  InformationService.before(hooks.before);

  // Set up our after hooks
  InformationService.after(hooks.after);
};

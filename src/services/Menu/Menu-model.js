'use strict';

// Menu-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MenuSchema = new Schema({
  _id: Number,
  name: { type: String, required: true },
  parrent_id: Number,
  url: String, // if it is a outlink
  type: String, 
  //text: { type: String, required: true },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

const MenuModel = mongoose.model('Menu', MenuSchema);

module.exports = MenuModel;
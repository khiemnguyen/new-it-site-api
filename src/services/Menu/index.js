'use strict';

const service = require('feathers-mongoose');
const Menu = require('./Menu-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: Menu,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/Menus', service(options));

  // Get our initialize service to that we can bind hooks
  const MenuService = app.service('/Menus');

  // Set up our before hooks
  MenuService.before(hooks.before);

  // Set up our after hooks
  MenuService.after(hooks.after);
};

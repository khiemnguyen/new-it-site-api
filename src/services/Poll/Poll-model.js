'use strict';

// Poll-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PollSchema = new Schema({
  _id: Number,
  des: { type: String, required: true },
  options: [
    {
      des: String,
      hits: Number
    }
  ],
  state: String,
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

const PollModel = mongoose.model('Poll', PollSchema);

module.exports = PollModel;
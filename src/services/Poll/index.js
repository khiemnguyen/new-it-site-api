'use strict';

const service = require('feathers-mongoose');
const Poll = require('./Poll-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: Poll,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/Polls', service(options));

  // Get our initialize service to that we can bind hooks
  const PollService = app.service('/Polls');

  // Set up our before hooks
  PollService.before(hooks.before);

  // Set up our after hooks
  PollService.after(hooks.after);
};

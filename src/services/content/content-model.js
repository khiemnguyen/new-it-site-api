'use strict';

// Content-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContentSchema = new Schema({
  _id: Number,
  title: {
    type: String,
    required: true
  },
  fullText: {
    type: String,
    required: true
  },
  state: String,
  author_id: Number,
  update_by: Number,
  image_url: String, // url
  hits: Number, // for count hit
  version: Schema.Types.Mixed, // for future feature like change history or target user post
  category_id: Number,
  comments: [{
    author_id: Number,
    comment_text: String,
    createdAt: {
      type: Date,
      'default': Date.now
    }
  }],
  attachment:[ {
    fileName: String,
    type: String, // MINE
    url: String
  }],
  //text: { type: String, required: true },
  createdAt: {
    type: Date,
    'default': Date.now
  },
  updatedAt: {
    type: Date,
    'default': Date.now
  }
});

const ContentModel = mongoose.model('Content', ContentSchema);

module.exports = ContentModel;

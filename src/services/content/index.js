'use strict';

const service = require('feathers-mongoose');
const Content = require('./Content-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: Content,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/Contents', service(options));

  // Get our initialize service to that we can bind hooks
  const ContentService = app.service('/Contents');

  // Set up our before hooks
  ContentService.before(hooks.before);

  // Set up our after hooks
  ContentService.after(hooks.after);
};

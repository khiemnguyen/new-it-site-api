'use strict';
const event = require('./Event');
const poll = require('./Poll');
const content = require('./Content');
const information = require('./Information');
const menu = require('./Menu');
const category = require('./Category');
const authentication = require('./authentication');
const user = require('./user');
const mongoose = require('mongoose');
module.exports = function() {
  const app = this;

  mongoose.connect(app.get('mongodb'));
  mongoose.Promise = global.Promise;

  app.configure(authentication);
  app.configure(user);
  app.configure(category);
  app.configure(menu);
  app.configure(information);
  app.configure(content);
  app.configure(poll);
  app.configure(event);
};

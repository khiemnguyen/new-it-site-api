'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('Category service', function() {
  it('registered the Categories service', () => {
    assert.ok(app.service('Categories'));
  });
});

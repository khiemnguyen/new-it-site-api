'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('Event service', function() {
  it('registered the Events service', () => {
    assert.ok(app.service('Events'));
  });
});

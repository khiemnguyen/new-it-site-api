'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('Information service', function() {
  it('registered the Information service', () => {
    assert.ok(app.service('Information'));
  });
});

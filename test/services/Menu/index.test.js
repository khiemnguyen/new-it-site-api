'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('Menu service', function() {
  it('registered the Menus service', () => {
    assert.ok(app.service('Menus'));
  });
});

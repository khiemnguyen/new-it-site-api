'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('Poll service', function() {
  it('registered the Polls service', () => {
    assert.ok(app.service('Polls'));
  });
});

'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('Content service', function() {
  it('registered the Contents service', () => {
    assert.ok(app.service('Contents'));
  });
});
